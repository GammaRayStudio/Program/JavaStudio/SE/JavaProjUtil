package com.enoxs.infra.util;


import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * FileUtil 封裝 util.io.File
 */
public class FileUtil {

    private final static byte[] fileBuffer = new byte[1024 * 5]; //ByteBuffer

    /**
     * 判斷檔案或資料夾是否存在
     */
    public static Boolean isExist(String path) {
        // Enoxs - Study : 傳入的參數
        // Enoxs - Fix : 重複代碼

        File file = new File(path);
        return file.exists();
    }


    /**
     * 檢查表示此抽象路徑名的文件是否是一個檔案
     */
    public static Boolean isFile(String path) {
        File file = new File(path);
        return file.isFile();
    }


    /**
     * 檢查表示此抽象路徑名的文件是否是一個目錄
     */
    public static Boolean isDir(String path) {
        File file = new File(path);
        return file.isDirectory();
    }

    /**
     * 儲存檔案
     */
    public static void save(String text, String path, String name) {
        File dir = new File(path);
        dir.mkdirs();

        File file = new File(path, name);

        byte data[] = text.getBytes();

        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(data);
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 載入檔案
     */
    public static String load(String path) {
        String text = "";
        File file = new File(path);

        if (isFile(path)) {
            try {
                byte[] bytesArray = new byte[(int) file.length()];

                FileInputStream fis = new FileInputStream(file);
                fis.read(bytesArray); //read file into bytes[]
                fis.close();

                text = new String(bytesArray);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("檔案不存在，請檢查");
        }

        return text;
    }


    /**
     * 創建資料夾
     */
    public static boolean make(String path) { // creste folder
        return new File(path).mkdirs();
    }

    /**
     * 創建檔案
     */
    public static boolean make(String path, String name) {// create file
        boolean isMake = false;

        // 創建資料夾
        File dir = new File(path);
        dir.mkdirs();

        // 創建檔案
        File file = new File(path, name);

        try {
            isMake = file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return isMake;
    }


    /**
     * 移動檔案或資料夾
     *
     * @param at : 來源
     * @param to : 目標
     */
    public static boolean move(String at, String to) {
        boolean isMove = false;
        File file = new File(to);
        String dirPath = file.getParent();

        File dir = new File(dirPath);
        dir.mkdirs();

        File fileAt = new File(at);
        File fileTo = new File(to);
        isMove = fileAt.renameTo(fileTo);
        return isMove;
    }


    /**
     * 複製檔案或資料夾
     *
     * @param at : 來源
     * @param to : 目標
     */
    public static boolean copy(String at, String to) {
        boolean isCopy = false;

        File fileAt = new File(at);
        File fileTo = new File(to);

        try {
            if (isFile(at)){
                FileUtils.copyFile(fileAt , fileTo);
            }
            if (isDir(to)){
                FileUtils.copyDirectory(fileAt , fileTo);
            }
            isCopy = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return isCopy;
    }


    /**
     * 刪除檔案或資料夾
     * @param path : 檔案或資料夾的路徑
     */
    public static boolean delete(String path) {
        boolean isDel = false;

        File file = new File(path);

        if (file.isFile()){
            isDel = file.delete();
        }

        if(file.isDirectory()){
            try {
                FileUtils.deleteDirectory(new File(path));
                isDel = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return isDel;
    }


    /**
     *
     * Android 功能 : 回傳該資料夾路徑下，所有的檔案與資料夾的「名稱」
     * iOS 的功能 : 回傳該資料夾路徑下，所有的檔案與資料夾的「絕對路徑」
     *
     * @param path : 目標資料夾的路徑
     * @return :
     *
     * + List.get(0) - 資料夾的陣列名稱
     * + List.get(1) - 檔案的陣列名稱
     *
     * list.size() == 2 - 資料夾存在，但裡面的檔案是空的
     * list.size() == 0 - 資料夾不存在
     *
     */
    public static List<String[]> listDirInfo(String path) {
        List<String[]> dirInfo = new LinkedList<>();

        File file = new File(path);

        if(file.exists() && file.isDirectory()) {
            List<String> lstDir = new LinkedList<>();
            List<String> lstFile = new LinkedList<>();

            File[] listFiles = file.listFiles();
            for (File f : listFiles) {
                if (f.isDirectory()) {
                    lstDir.add(f.getName());
                }
                if (f.isFile()) {
                    lstFile.add(f.getName());
                }
            }

            String[] dirArr = lstDir.toArray(new String[0]);
            String[] fileArr = lstFile.toArray(new String[0]);

            Arrays.sort(dirArr);
            Arrays.sort(fileArr);

            dirInfo.add(dirArr);
            dirInfo.add(fileArr);
        }

        return dirInfo;
    }

}

