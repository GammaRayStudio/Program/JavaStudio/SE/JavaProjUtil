package com.enoxs.infra.util;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileUtilTest {
    FileUtil fileUtil = new FileUtil();
    String slash = File.separator; // 右 - 斜線 , 左 - 倒斜

    // 相對位置
    String docPath = "assets" + slash + "FileUtil";



    @Test
    public void testIsExistFileTrue() {
        createTemplateConfigFile();
        String filePath = "/SysConfig/template-config.md";
        String fullPath = docPath + filePath;


        boolean isBool = fileUtil.isExist(fullPath);
        assertTrue(isBool);

        delete(fullPath);
    }

    @Test
    public void testIsExistFileFalse() {
        createTemplateConfigFile();
        String filePath = "/SysConfig/template-config.md";
        String fullPath = docPath + filePath;


        delete(fullPath);

        boolean isBool = fileUtil.isExist(fullPath);
        assertFalse(isBool);
    }


    @Test
    public void testIsExistDirTrue(){
        String filePath = "/SysConfig";
        String fullPath = docPath + filePath;

        createDir(fullPath);

        boolean isBool = fileUtil.isExist(fullPath);

        assertTrue(isBool);
        delete(fullPath);
    }

    @Test
    public void testIsExistDirFalse(){
        String filePath = "/SysConfig";
        String fullPath = docPath + filePath;

        delete(fullPath);

        boolean isBool = fileUtil.isExist(fullPath);

        assertFalse(isBool);
    }


    @Test
    public void testIsFileAtFileTrue(){
        createTemplateConfigFile();

        String filePath = "/SysConfig/template-config.md";
        String fullPath = docPath + filePath;

        boolean isBool = fileUtil.isFile(fullPath);


        assertTrue(isBool);
        delete(fullPath);
    }

    @Test
    public void testIsFileAtFileFalse(){
        String filePath = "/SysConfig/template-config.md";
        String fullPath = docPath + filePath;

        delete(fullPath);
        boolean isBool = fileUtil.isFile(fullPath);

        assertFalse(isBool);
    }

    @Test
    public void testIsFileAtDirFalse(){
        String filePath = "/SysConfig";
        String fullPath = docPath + filePath;
        createDir(fullPath);

        boolean isBool = fileUtil.isFile(fullPath);

        assertFalse(isBool);
        delete(fullPath);
    }


    @Test
    public void testIsDirAtFileFalse(){
        createTemplateConfigFile();

        String filePath = "/SysConfig/template-config.md";
        String fullPath = docPath + filePath;
        boolean isBool = fileUtil.isDir(fullPath);

        assertFalse(isBool);
        delete(fullPath);
    }

    @Test
    public void testIsDirAtDirTrue(){
        String filePath = "/SysConfig";
        String fullPath = docPath + filePath;
        createDir(fullPath);

        boolean isBool = fileUtil.isDir(fullPath);

        assertTrue(isBool);
        delete(fullPath);
    }

    @Test
    public void testIsDirAtDirFalse(){
        String filePath = "/SysConfig";
        String fullPath = docPath + filePath;
        delete(fullPath);

        boolean isBool = fileUtil.isDir(fullPath);

        assertFalse(isBool);
    }

    @Test
    public void testSave(){
        String filePath = "/SysConfig/template-config.md";
        String fullPath = docPath + filePath;
        delete(fullPath);

        String fileName = "template-config.md";
        String fileText = "JavaProjUtil\n======";
        String fileDir = docPath + slash + "SysConfig";

        fileUtil.save(fileText ,fileDir ,fileName);

        boolean isBool = checkFileAndFoldIsExist(fullPath);

        assertTrue(isBool);
        delete(fullPath);
    }

    @Test
    public void testLoadText(){
        write("hello world", "actual.md");

        String filePath = "/actual.md";
        String fullPath = docPath + filePath;

        String fileText = fileUtil.load(fullPath);
        System.out.println("fileText -> " + fileText);

        assertEquals("hello world" , fileText);
        delete(fullPath);
    }

    @Test
    public void testLoadEmpty(){
        String filePath = "/Actual";
        String fullPath = docPath + filePath;

        createDir(fullPath);

        String fileText = fileUtil.load(fullPath);

        assertEquals("",fileText);
    }

    @Test
    public void testMakeDirTrue(){
        String filePath = "/SysConfig";
        String fullPath = docPath + filePath;
        delete(fullPath);

        boolean isBool = fileUtil.make(fullPath);
        assertTrue(isBool);
    }

    @Test
    public void testMakeFileTrue(){
        String filePath = "/SysConfig";
        String fileName = "template-config.md";
        String fullPath = docPath + filePath;
        delete(fullPath);

        boolean isBool = fileUtil.make(fullPath , fileName);
        assertTrue(isBool);
    }

    @Test
    public void testMoveFile(){
        String filePath = "/Target/tempalte.md";
        String fullPath = docPath + filePath;

        createTemplateConfigFile();
        delete(fullPath);

        String fromPath = docPath + "/SysConfig/template-config.md";
        String toPath = docPath + "/Target/template.md";
        boolean isBool = fileUtil.move(fromPath, toPath);

        assertTrue(isBool);
        delete(toPath);
    }

    @Test
    public void testMoveDir(){
        String filePath = "/Target/SubTarget";
        String fullPath = docPath + filePath;

        createTemplateConfigFile();
        delete(fullPath);

        String fromPath = docPath + "/SysConfig";
        String toPath = docPath + "/Target/SubTarget";
        boolean isBool = fileUtil.move(fromPath, toPath);

        assertTrue(isBool);
        delete(toPath);
    }


    @Test
    public void testCopyFile(){
        String filePath = "/Target/template.md";
        String fullPath = docPath + filePath;

        createTemplateConfigFile();
        delete(fullPath);

        String fromPath = docPath + "/SysConfig/template-config.md";
        String toPath = docPath + "/Target/template.md";
        boolean isBool = fileUtil.copy(fromPath,toPath);

        assertTrue(isBool);
        delete(docPath + "/Target");
    }


    @Test
    public void testCopyDir(){
        String filePath = "/Target/SubTarget/template.md";
        String fullPath = docPath + filePath;

        createTemplateConfigFile();
        delete(fullPath);

        String fromPath = docPath + "/SysConfig/template-config.md";
        String toPath = docPath + "/Target/SubTarget/template.md";

        boolean isBool = fileUtil.copy(fromPath,toPath);

        assertTrue(isBool);

        delete(docPath + "/Target");
    }

    @Test
    public void testDeleteFile(){
        createFile(docPath + "/Target" ,"template.md");

        String path = docPath + "/Target/template.md";
        boolean isBool = fileUtil.delete(path);
        assertTrue(isBool);

        boolean isFalse = checkFileAndFoldIsExist(path);
        assertFalse(isFalse);
    }

    @Test
    public void testDeleteDir(){
        String path = docPath + "/Target";

        createDir(path);

        boolean isBool = fileUtil.delete(path);
        assertTrue(isBool);


        boolean isFalse = checkFileAndFoldIsExist(path);
        assertFalse(isFalse);
    }

    @Test
    public void testListDirInfoExist(){
        createFile(docPath + "/Template" , "first.md");
        createFile(docPath + "/Template" , "second.md");
        createFile(docPath + "/Template" , "three.md");
        createFile(docPath + "/Template/Custom" , "custom.md");
        createDir(docPath + "/Template/CurrentAccount");

        List<String[]> lstExpect = new LinkedList<>();

        String [] dirArr = {"CurrentAccount" , "Custom"};
        String [] fileArr = {"first.md" , "second.md" , "three.md"};

        Arrays.sort(dirArr);
        Arrays.sort(fileArr);

        lstExpect.add(dirArr);
        lstExpect.add(fileArr);

        List<String []> lstActual = FileUtil.listDirInfo(docPath + "/Template");


        // 資料夾與檔案的陣列
        assertEquals(2 , lstActual.size());

        if(lstActual.size() > 0){
            // Check : Directory Name
            String [] expect = lstExpect.get(0);
            String [] actual = lstActual.get(0);
            if(actual.length > 0){
                for (int i = 0; i < actual.length; i++) {
                    assertEquals(expect[i] , actual[i]);
                }
            }else{
                boolean isFalse = checkFileAndFoldIsExist(docPath + "/Template");
                assertFalse(isFalse);
            }

            // Check : File Name
            if(actual.length > 0){
                expect = lstExpect.get(1);
                actual = lstActual.get(1);
                for (int i = 0; i < actual.length; i++) {
                    assertEquals(expect[i] , actual[i]);
                }
            }else{
                boolean isFalse = checkFileAndFoldIsExist(docPath + "/Template");
                assertFalse(isFalse);
            }
        }

        delete(docPath + "/Template");
    }

    @Test
    public void testListDirInfoNotExist(){
        delete(docPath + "/Template");
        createDir(docPath + "/Template");


        List<String []> lstActual = FileUtil.listDirInfo(docPath + "/Template");
        assertEquals(2 , lstActual.size());


        delete(docPath + "/Template");

        lstActual = FileUtil.listDirInfo(docPath + "/Template");
        assertEquals(0 , lstActual.size());

        createFile(docPath , "Template");
        lstActual = FileUtil.listDirInfo(docPath + "/Template");
        assertEquals(0 , lstActual.size());

        delete(docPath + "/Template");
    }

    // - - - 分隔線 - - -
    private void createTemplateConfigFile() {
        String fileDir = slash + "SysConfig";
        String folderURL = docPath + fileDir;
        String fileName = "template-config.md";

        System.out.println(folderURL);

        // 創建資料夾
        File folder = new File(folderURL);
        folder.mkdirs();

        // 創建目標的測試檔案
        File file = new File(folderURL,fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createDir(String path){
        File file = new File(path);
        file.mkdirs();
    }

    private void createFile(String path , String name){
        File dir = new File(path);
        dir.mkdirs();

        File file = new File(path , name);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private boolean checkFileAndFoldIsExist(String path){
        return new File(path).exists();
    }

    private void write(String text, String name){
        File file = new File(docPath , name);

        byte data [] = text.getBytes();

        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(data);
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void delete(String path){
        File file = new File(path);
        if (file.isFile()){
            file.delete();
        }

        if(file.isDirectory()){
            try {
                FileUtils.deleteDirectory(new File(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }



}